<?php include('header.php'); ?>

<div class="header_wrapper">
	<div class="login">
          <?php
				$today = date("F j, Y");
				echo '&nbsp;Today is '.$today;
				?>
                &nbsp;&nbsp;&nbsp;<a href="FeedbackForm.php">Submit Feedback</a>
            <ul>
            	
                <li><a href="../loginpage.php">Admin Login</a></li>
            </ul>
  </div>
</div>
<!--Start Menu-->
<div class="header_menu">
	<div class="menu">
    	<ul>
        	<li><a href="../index.php">HOME</a></li>
            <li><a href="Albums.php">ALBUMS</a></li>
            <li><a href="Licensing.php">LICENSING</a></li>
            <li><a href="Songs.php">VOTE</a></li>
            <li><a href="AboutUs.php">ABOUT US</a></li>
            <li><a href="News.php">NEWS</a></li>
            
    	</ul>
    </div>
</div>
<!--End Menu-->
<div class="header_under"></div>
	<!--Start Container for the web content-->
	<div class="playlist_wrapper">
    	<div class="submenu">
        	<ul>     
            	<li><a href="Licensing.php">Licensing</a></li>
                <li><a href="Advertisement.php">Advertisement</a></li>
                <li><a href="Merchandising.php">Merchandising</a></li>          
            </ul>
        </div><!--End Submenu-->
        <div class="pcontainer">
        <div class="search_box"></div>
        	<h3 style="color:#06F;">Advertising</h3>
            <hr />
        	<p>With the fragmentation of media making it more and more difficult for brands to reach out to their target market effectively, the right piece of music can be the difference between success and failure. But getting the perfect song for your creative need can be difficult, especially when you’ve got the client on the phone screaming for the latest cut. That’s when working with E-Music Publishing can pay dividends.</p> 
            <p>Our team of advertising specialists helps take all of the hassle and stress out of music licensing. We’ve got a proven track record in commercials, working on everything from the biggest global ads to small local campaigns. As a result, we’re experienced in quickly and effectively overcoming any licensing hurdles, to ensure that your project stays on time – and on budget.</p>
            <p>With 1.3 million songs to choose from, we’re certain to have a piece of music that will meet your creative needs. Of course, navigating your way through all that music isn’t easy. That’s why we’ve got a highly trained team of music experts on hand, to create highly tailored solutions with your requirements specifically in mind.</p>
            <p>No music company understands brands better than E-Music Publishing, and when it comes to advertising, there isn’t a problem that we haven’t got a solution for. Called by a client on a Friday afternoon, needing a new soundtrack for a commercial that’s airing the following afternoon? Our pre-cleared and easy-to-clear songs will ensure that you’ll have everything resolved within hours, leaving you free to get your weekend started. Or maybe you’re looking for something custom-made to reflect the unique qualities of your business and its products or services? We can work with our roster of talented writers and composers to make that happen too.</p>
            <p>Every great moment deserves a great song. Start your next advertising project with the world’s greatest catalog of songs at your fingertips, with E-Music Publishing.</p>
        </div>
	</div><!--End Container-->
<?php include('footer.php'); ?>