<?php include('header.php'); ?>

<div class="header_wrapper">
	<div class="login">
          <?php
				$today = date("F j, Y");
				echo '&nbsp;Today is '.$today;
				?>
                &nbsp;&nbsp;&nbsp;<a href="FeedbackForm.php">Submit Feedback</a>
            <ul>   
           		   	
                <li><a href="../loginpage.php">Admin Login</a></li>
            </ul>
   	</div></div>

<div class="header_menu"><!--Start Menu-->
	<div class="menu">
    	<ul>
        	<li><a href="../index.php">HOME</a></li>
            <li><a href="Albums.php">ALBUMS</a></li>
            <li><a href="Licensing.php">LICENSING</a></li>
            <li><a href="Songs.php">VOTE</a></li>
            <li><a href="AboutUs.php">ABOUT US</a></li>
            <li><a href="News.php">NEWS</a></li>
            
    	</ul>
    </div>
</div><!--End Menu-->
<div class="header_under"></div>
<div class="container_wrapper"><!--Start Container for the web content-->
    <div class="sidebar_menu"><!--Sidebar-->
    	<h3 class="header_1">E-Music</h3>
            <ul>
            	<li><a href="History.php">History</a></li>
                <li><a href="Profile.php">Company Profile</a></li>
                <li><a href="Contacts.php">Contact Us</a></li>
                <li><a href="Careers.php">Careers</a></li>
            </ul>
    </div><!--End sidebar-->
    <div class="col2"><!--Start second column-->
   
     	<div id="header_title">E-Music History</div>
        	<div class="content1_info">
				<p>E-Music is a global music publishing company and contains one of the world's greatest collections of musical compositions, ranging from well-known standards to new songs by emerging artists.</p>	
                <p>E-Music is a leader in the creation of innovative strategies for the marketing and promotion of songwriters and their music and, in 2007, won its sixth consecutive SESAC "Publisher of the Year Award." That same year, E-Music announced a partnership with rock band Radiohead to create a first-of-its-kind rights clearance strategy for the digital release of the album, In Rainbows. In 2006, the company launched its Pan-European Digital Licensing (PEDL) initiative, which is driving collection societies across Europe to adopt simpler, one-stop licenses for E-Music songs used by digital music services.</p>
                <p>E-Music is a leader in the creation of innovative strategies for the marketing and promotion of songwriters and their music and, in 2007, won its sixth consecutive SESAC "Publisher of the Year Award." That same year, E-Music announced a partnership with rock band Radiohead to create a first-of-its-kind rights clearance strategy for the digital release of the album, In Rainbows. In 2006, the company launched its Pan-European Digital Licensing (PEDL) initiative, which is driving collection societies across Europe to adopt simpler, one-stop licenses for E-Music songs used by digital music services.</p>
                <p>E-Music dates its origins back to 1811 and the formation of Chappell & Company, a music publishing house and instrument store on London's Bond Street, making it the oldest company in the WMG family. Its legendary music publishing catalog makes E-Music a natural first stop for A&R executives and record producers, feature film and television production companies and others looking to record or license some of the world's greatest music. We also administer the music and soundtracks of several third-party television and film producers and studios, including Lucasfilm, Ltd., Hallmark Entertainment and Disney Music Publishing.</p>
        	</div>
    </div><!--End second column-->
</div>
<?php include('footer.php'); ?>