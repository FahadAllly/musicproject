<?php include('header.php'); ?>

<div class="header_wrapper">
	<div class="login">
          <?php
				$today = date("F j, Y");
				echo '&nbsp;Today is '.$today;
				?>
                &nbsp;&nbsp;&nbsp;<a href="FeedbackForm.php">Submit Feedback</a>
            <ul>
            	
               <li><a href="../loginpage.php">Admin Login</a></li>
            </ul>
   	</div>
</div>

<div class="header_menu"><!--Start Menu-->
	<div class="menu">
    	<ul>
        	<li><a href="../index.php">HOME</a></li>
            <li><a href="Genere.php">GENERE</a></li>
            <li><a href="Licensing.php">LICENSING</a></li>
            <li><a href="Songs.php">VOTE</a></li>
            <li><a href="AboutUS.php">ABOUT US</a></li>
            <li><a href="News.php">NEWS</a></li>
            
    	</ul>
    </div>
</div><!--End Menu-->
<div class="header_under"></div>
<div class="container_wrapper"><!--Start Container for the web content-->
    <div class="sidebar_menu"><!--Sidebar-->
    	<h3 class="header_1">E-Music</h3>
            <ul>
            	<li><a href="History.php">History</a></li>
                <li><a href="Profile.php">Company Profile</a></li>
                <li><a href="Contacts.php">Contact Us</a></li>
                <li><a href="Careers.php">Careers</a></li>
            </ul>
    </div><!--End sidebar-->
    <div class="col2"><!--Start second column-->
   
     	<div id="header_title">Contact Us</div>
		<div class="content1_info">
        	<h3>Office</h3>
            <p>Virtual University </p>
            <p>Iqbal Town</p>
            <p>Lahore</p>
            <p></p>
        	<table>
            	<tr>
                	<th>Student ID</th><th>Supervisor</th>
                </tr>
                <tr>
                	<td>Fahad Ali</td><td>Maham Murtaza Khan</td>
                </tr>
                <tr>
                	<td>&nbsp;</td><td>&nbsp;</td>
                </tr>
                <tr>
                	<th>Contact Numbers</th><th></th>
                </tr>
                <tr>
                	<td>Email</td><td>E-Musicmusic@yahoo.com</td>
                </tr>
                <tr>
                	<td>Fax No.</td><td>+000.000.0000</td>          
                </tr>
                <tr>
                	<td>Tel No.</td><td>000-0000</td>          
                </tr>
            </table>
        </div>
    </div><!--End second column-->
</div>
<?php include('footer.php'); ?>